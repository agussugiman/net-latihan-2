﻿using System.ComponentModel.DataAnnotations;

namespace latihan_netcore.Models
{
    public class ModelProduct
    {
        [Required]
        public int? IDCompany { get; set; }

        [Required]
        public int? IDBrand { get; set; }

        [Required]
        public string? Name { get; set; }

        [Required]
        public string? Variant { get; set; }
        
        [Required]
        public double? Price { get; set; }

        [Required]
        public int? IDUser { get; set; }

        //public DateTime Date { get; set; } = DateTime.Now;
    }
}
