﻿using System.ComponentModel.DataAnnotations;

namespace latihan_netcore.Models
{
    public class ModelCompany
    {
        [Required]
        public string? Name { get; set; }
        
        public string? Address { get; set; }
        
        public string? Telephone { get; set; }
        
        public DateTime Date { get; set; } = DateTime.Now;
    }
}
