﻿using Dapper;
using latihan_netcore.Models;

namespace latihan_netcore.Services.Interface
{
    public interface IProduct
    {
        Response<T> Execute_Command<T>(string query, DynamicParameters sp_params);
        Response<List<T>> getProductList<T>();
    }
}
