﻿using Dapper;
using latihan_netcore.Models;
using latihan_netcore.Services.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
namespace latihan_netcore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CompanyController : ControllerBase
    {
        private readonly ICompany _company;
        public CompanyController(ICompany company)
        {
            _company = company;
        }

        [HttpGet("company-list")]
        [Authorize(Roles = "    c  ")]
        public IActionResult getcompany()
        {
            var result = _company.getCompanyList<ModelCompany>();

            return Ok(result);
        }

        [HttpGet("company-list-by-id")]
        [Authorize(Roles = "Admin")]
        public IActionResult getcompanybyid(int ID)
        {
            var result = _company.getCompanybyID<ModelCompany>(ID);

            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

            //return BadRequest(result);
        }

        [HttpPost("create-company")]
        [Authorize(Roles = "Admin")]
        public IActionResult Register([System.Web.Http.FromBody] ModelCompany company)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Parameter is missing");
            }

            DynamicParameters dp_param = new DynamicParameters();
            dp_param.Add("name", company.Name, DbType.String);
            dp_param.Add("address", company.Address, DbType.String);
            dp_param.Add("telephone", company.Telephone, DbType.String);
            dp_param.Add("retVal", DbType.String, direction: ParameterDirection.Output);

            var result = _company.Execute_Command<ModelCompany>("sp_CreateCompany", dp_param);
            if (result.Code == 200)
            {
                return Ok(new { Data = company });
            }

            return BadRequest(result);
        }

        [HttpPut("update-company")]
        [Authorize(Roles = "Admin")]
        public IActionResult Update([System.Web.Http.FromBody] ModelCompany company, string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Parameter is missing");
            }

            DynamicParameters dp_param = new DynamicParameters();
            dp_param.Add("ID", id, DbType.String);
            dp_param.Add("name", company.Name, DbType.String);
            dp_param.Add("address", company.Address, DbType.String);
            dp_param.Add("telephone", company.Telephone, DbType.String);
            dp_param.Add("retVal", DbType.String, direction: ParameterDirection.Output);

            var result = _company.Execute_Command<ModelCompany>("sp_UpdateCompany", dp_param);
            if (result.Code == 200)
            {
                return Ok(new { Data = company });
            }

            return BadRequest(result);
        }
    }
}
