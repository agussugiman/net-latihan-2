﻿using latihan_netcore.Models;
using latihan_netcore.Services.Interface;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System.Data;
using Dapper;
using latihan_netcore.Services.Repository;
using System.Xml.Linq;

namespace latihan_netcore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IProduct _product;
        public ProductController(IProduct product)
        {
            _product = product;
        }

        [HttpGet("product-list")]
        [Authorize(Roles = "Admin")]
        public IActionResult getProduct()
        {
            var result = _product.getProductList<ModelProduct>();

            return Ok(result);
        }

        [HttpPost("create-product")]
        [Authorize(Roles = "Admin")]
        public IActionResult Register([FromBody] ModelProduct product)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Parameter is missing");
            }

            DynamicParameters dp_param = new DynamicParameters();
            dp_param.Add("idcompany", product.IDCompany, DbType.String);
            dp_param.Add("idbrand", product.IDBrand, DbType.String);
            dp_param.Add("name", product.Name, DbType.String);
            dp_param.Add("varian", product.Variant, DbType.String);
            dp_param.Add("price", product.Price, DbType.String);
            dp_param.Add("iduser", product.IDUser, DbType.String);
            dp_param.Add("retVal", DbType.String, direction: ParameterDirection.Output);

            var result = _product.Execute_Command<ModelUser>("sp_CreateProduct", dp_param);
            if (result.Code == 200)
            {
                return Ok(new { datajson = product });
            }

            return BadRequest(result);
        }

        [HttpPut("update-product")]
        [Authorize(Roles = "Admin")]
        public IActionResult Update([FromBody] ModelProduct product, string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Parameter is missing");
            }

            DynamicParameters dp_param = new DynamicParameters();
            dp_param.Add("ID", id, DbType.String);
            dp_param.Add("idcompany", product.IDCompany, DbType.String);
            dp_param.Add("idbrand", product.IDBrand, DbType.String);
            dp_param.Add("name", product.Name, DbType.String);
            dp_param.Add("varian", product.Variant, DbType.String);
            dp_param.Add("price", product.Price, DbType.String);
            dp_param.Add("iduser", product.IDUser, DbType.String);
            dp_param.Add("retVal", DbType.String, direction: ParameterDirection.Output);

            var result = _product.Execute_Command<ModelProduct>("sp_UpdateProduct", dp_param);
           
            if (result.Code == 200)
            {
                return Ok(result);
            }

            return BadRequest(result);
        }
    }
}
